<!-- #region Jssor Slider Begin -->
<!-- Generator: Jssor Slider Maker -->
<!-- Source: http://www.jssor.com -->
<!-- This code works with jquery library. -->

<script type="text/javascript">
    jQuery(document).ready(function ($) {

        var jssor_1_options = {
          $AutoPlay: true,
          $ArrowNavigatorOptions: {
            $Class: $JssorArrowNavigator$
          },
          $ThumbnailNavigatorOptions: {
            $Class: $JssorThumbnailNavigator$,
            $Cols: 5,
            $SpacingX: 5,
            $SpacingY: 5,
            $Orientation: 2,
            $Align: 0
          }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*responsive code begin*/
        /*remove responsive code if you don't want the slider scales while window resizing*/
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 1100);
                jssor_1_slider.$ScaleWidth(refSize);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }
        ScaleSlider();
        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        /*responsive code end*/
    });
</script>

<div class="row">
  <div class="box">
<div class="container-fluid">



<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1100px;height:500px;overflow:hidden;visibility:hidden;background-color:#000000;">
    <!-- Loading Screen -->
    <div data-u="loading" style="position:absolute;top:0px;left:0px;background-color:rgba(0,0,0,0.7);">
        <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
        <div style="position:absolute;display:block;background:url('image/slider/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
    </div>
    <div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:800px;height:500px;overflow:hidden;">
        <div>
            <img data-u="image" src="image/slider/001.jpg" />
            <div data-u="thumb">
                <img class="i" src="image/slider/thumb-001.jpg" />
                <div class="t">The Workshop</div>
                <div class="c">Lầu 2, 27 Ngô Đức Kế, Quận 1</div>
            </div>
        </div>
        <div>
            <img data-u="image" src="image/slider/002.jpg" />
            <div data-u="thumb">
                <img class="i" src="image/slider/thumb-002.jpg" />
                <div class="t">L'USINE </div>
                <div class="c">151/1 Đồng Khởi, Quận 1 và 70B Lê Lợi, Quận 1 </div>
            </div>
        </div>
        <div>
            <img data-u="image" src="image/slider/003.jpg" />
            <div data-u="thumb">
                <img class="i" src="image/slider/thumb-003.jpg" />
                <div class="t">Khanhcasa Tea House</div>
                <div class="c">46 50 Đồng Khởi, Quận 1</div>
            </div>
        </div>
        <div>
            <img data-u="image" src="image/slider/004.jpg" />
            <div data-u="thumb">
                <img class="i" src="image/slider/thumb-004.jpg" />
                <div class="t">Heritage Coffee & Clothes</div>
                <div class="c">10 Pasteur, Quận 1</div>
            </div>
        </div>
        <div>
            <img data-u="image" src="image/slider/005.jpg" />
            <div data-u="thumb">
                <img class="i" src="image/slider/thumb-005.jpg" />
                <div class="t">Bâng Khuâng Café</div>
                <div class="c">Lầu 2, 9 Thái Văn Lung, Quận 1</div>
            </div>
        </div>
        <div>
            <img data-u="image" src="image/slider/006.jpg" />
            <div data-u="thumb">
                <img class="i" src="image/slider/thumb-006.jpg" />
                <div class="t">Themes</div>
                <div class="c">30+ professional themems + growing</div>
            </div>
        </div>
        <div>
            <img data-u="image" src="image/slider/007.jpg" />
            <div data-u="thumb">
                <img class="i" src="image/slider/thumb-007.jpg" />
                <div class="t">C.On Cafe</div>
                <div class="c">23B Ngô Thời Nhiệm, Quận 1</div>
            </div>
        </div>
        <a data-u="any" href="http://www.jssor.com" style="display:none">List Slider</a>
        <div>
            <img data-u="image" src="image/slider/006.jpg" />
            <div data-u="thumb">
                <img class="i" src="image/slider/thumb-006.jpg" />
                <div class="t">The Morning Cafe</div>
                <div class="c">Lầu 2, 36 Lê Lợi, Quận 1</div>
            </div>
        </div>
    </div>
    <!-- Thumbnail Navigator -->
    <div data-u="thumbnavigator" class="jssort11" style="position:absolute;right:5px;top:0px;font-family:Arial, Helvetica, sans-serif;-moz-user-select:none;-webkit-user-select:none;-ms-user-select:none;user-select:none;width:295px;height:500px;" data-autocenter="2">
        <!-- Thumbnail Item Skin Begin -->
        <div data-u="slides" style="cursor: default;">
            <div data-u="prototype" class="p">
                <div data-u="thumbnailtemplate" class="tp"></div>
            </div>
        </div>
        <!-- Thumbnail Item Skin End -->
    </div>
    <!-- Arrow Navigator -->
    <span data-u="arrowleft" class="jssora02l" style="top:0px;left:8px;width:55px;height:55px;" data-autocenter="2"></span>
    <span data-u="arrowright" class="jssora02r" style="top:0px;right:318px;width:55px;height:55px;" data-autocenter="2"></span>
</div>
<!-- #endregion Jssor Slider End -->
</div>
</div>
</div>
