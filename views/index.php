<!DOCTYPE html>
<html lang="vi">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Review Coffee</title>
    <!-- jQuery -->
    <script src="../public/js/jquery-3.2.0.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link href="../public/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../public/css/style.css" rel="stylesheet">
    <link href="../public/css/header.css" rel="stylesheet">
    <link href="../public/css/navbar.css" rel="stylesheet">
    <link href="../public/css/slider.css" rel="stylesheet">
    <link href="../public/css/content.css" rel="stylesheet">
    <link href="../public/css/footer.css" rel="stylesheet">
    <link href="../public/css/register.css" rel="stylesheet">
    <link href="../public/css/blog.css" rel="stylesheet">
    <link href="../public/css/blog-content.css" rel="stylesheet">
    <link href="../public/css/map-places.css" rel="stylesheet">
    <link href="../public/css/place-details.css" rel ="stylesheet" >
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>

    <?php
    include './layout/header.php';
    include './layout/navbar.php'
    ?>

    <div class="container">

        <?php
            include './layout/content.php'
         ?>

    </div>
    <!-- /.container -->
    <?php
          include('./layout/register.php');
          include('./layout/footer.php')
     ?>

  <style media="screen">
  .go_top {
  position: fixed;
  z-index: 50;
  width: 65px;
  height: 65px;
  right: 10px;
  bottom: 40px;
  overflow: hidden;
  background: url(http://coffeR/public/image/back_top.jpg) no-repeat;
}
  </style>
    <a href="javascript:void(0)" onclick="jQuery('html,body').animate({scrollTop: 0},1000);" class="go_top" style=""></a>
    <!-- Bootstrap Core JavaScript -->
    <script src="../public/bootstrap/js/bootstrap.min.js"></script>
    <script src="../js/banner/jssor.slider-22.2.16.mini.js" type="text/javascript"></script>
    <!-- Insert to your webpage before the </head> -->
       <script src="http://uguru-realestate-us-jun202013.businesscatalyst.com/3d-slider/sliderengine/jquery.js"></script>
       <script src="http://uguru-realestate-us-jun202013.businesscatalyst.com/3d-slider/sliderengine/amazingslider.js"></script>
       <script src="http://uguru-realestate-us-jun202013.businesscatalyst.com/3d-slider/sliderengine/initslider-1.js"></script>
       <!-- End of head section HTML codes -->
</body>

</html>
