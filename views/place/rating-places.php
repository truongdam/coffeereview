<?php
if(!isset($_SESSION)){
    session_start();
}
if(!isset($_SESSION['user_id'])){
  header("location:../account/login_register.php");
};
 ?>

<!DOCTYPE html>
<html lang="vi">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Review Coffee</title>
    <!-- jQuery -->
    <script src="../../public/js/jquery-3.2.0.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link href="../../public/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../public/css/style.css" rel="stylesheet">
    <link href="../../public/css/header.css" rel="stylesheet">
    <link href="../../public/css/navbar.css" rel="stylesheet">
    <link href="../../public/css/slider.css" rel="stylesheet">
    <link href="../../public/css/content.css" rel="stylesheet">
    <link href="../../public/css/footer.css" rel="stylesheet">
    <link href="../../public/css/register.css" rel="stylesheet">
    <link href="../../public/css/blog.css" rel="stylesheet">
    <link href="../../public/css/blog-content.css" rel="stylesheet">
    <link href="../../public/css/map-places.css" rel="stylesheet">
    <link href="../../public/css/place-details.css" rel ="stylesheet" >
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style media="screen">
    .rating {
        margin-left: 30px;
    }

    div.skill {
        background: #5cb85c;
        border-radius: 3px;
        color: white;
        font-weight: bold;
        padding: 3px 4px;
        /*width: 70px;*/
    }

    .skillLine {
        display: inline-block;
        width: 100%;
        min-height: 90px;
        padding: 3px 4px;
    }

    .skillLineDefault {
        padding: 3px 4px;
    }
    .list-group-item{
        min-height: 50px;
    }


     .star-rating{
        font-size: 21px;
        float: right;
        margin-right: 5px;
        color: #ff5722;
    }
    </style>
    <script language="javascript">

    </script>

</head>
<?php
include '../layout/header.php'

 ?>
 <?php include '../layout/navbar.php' ?>
    <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="well well-sm">
          <form action="../../controller/rating-places.php" method="POST">
          <div class="panel panel-default">
              <div class="panel-heading">
                  <input type="hidden" name="store_id" id="store_id" value="<?php echo  $_GET['store']?>">
                  <h4 class="text-center">Xếp hạng: <?php if(isset($_GET['name_store'])) echo $_GET['name_store'] ?><span class="glyphicon glyphicon-saved pull-right"></span></h4>
              </div>
              <ul class="list-group list-group-flush text-center">
                  <li class="list-group-item">
                      <div class="skillLineDefault">
                          <div class="skill pull-left text-center">Không gian</div>
                          <div class="star-rating">
                              <div id="stars1" class="starrr"><input type="hidden" name="khong_gian" id="khong_gian" value=""></div>
                          </div>
                      </div>
                  </li>
                  <li class="list-group-item text-center">
                      <div class="skillLineDefault">
                          <div class="skill pull-left text-center">Giá cả</div>
                           <div class="star-rating">
                              <div id="stars2" class="starrr"><input type="hidden" name="gia_ca" id="gia_ca" value=""></div>
                          </div>
                      </div>
                  </li>
                  <li class="list-group-item text-center">
                      <div class="skillLineDefault">
                          <div class="skill pull-left text-center">Phục vụ</div>
                           <div class="star-rating">
                              <div id="stars3" class="starrr"><input type="hidden" name="phuc_vu" id="phuc_vu" value=""></div>
                          </div>
                      </div>
                  </li>
                  <li class="list-group-item text-center">
                      <div class="skillLineDefault">
                          <div class="skill pull-left text-center">Chất lượng</div>
                           <div class="star-rating">
                              <div id="stars4" class="starrr"><input type="hidden" name="chat_luong" id="chat_luong" value=""></div>
                          </div>
                      </div>
                  </li>
              </ul>
              <div class="panel-footer text-center">
                  <button type="submit" class="btn btn-primary btn-lg btn-block">
                      Đánh giá
                  </button>
              </div>
          </div>
        </form>

        </div>
     </div>
    </div>
    <script src="../../public/bootstrap/js/bootstrap.min.js"></script>

    <script type="text/javascript">
        // Starrr plugin (https://github.com/dobtco/starrr)
            var __slice = [].slice;

            (function($, window) {
              var Starrr;

              Starrr = (function() {
                Starrr.prototype.defaults = {
                  rating: void 0,
                  numStars: 5,
                  change: function(e, value) {}
                };

                function Starrr($el, options) {
                  var i, _, _ref,
                    _this = this;

                  this.options = $.extend({}, this.defaults, options);
                  this.$el = $el;
                  _ref = this.defaults;
                  for (i in _ref) {
                    _ = _ref[i];
                    if (this.$el.data(i) != null) {
                      this.options[i] = this.$el.data(i);
                    }
                  }
                  this.createStars();
                  this.syncRating();
                  this.$el.on('mouseover.starrr', 'span', function(e) {
                    return _this.syncRating(_this.$el.find('span').index(e.currentTarget) + 1);
                  });
                  this.$el.on('mouseout.starrr', function() {
                    return _this.syncRating();
                  });
                  this.$el.on('click.starrr', 'span', function(e) {
                    return _this.setRating(_this.$el.find('span').index(e.currentTarget) + 1);
                  });
                  this.$el.on('starrr:change', this.options.change);
                }

                Starrr.prototype.createStars = function() {
                  var _i, _ref, _results;

                  _results = [];
                  for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
                    _results.push(this.$el.append("<span class='glyphicon .glyphicon-star-empty'></span>"));
                  }
                  return _results;
                };

                Starrr.prototype.setRating = function(rating) {
                  if (this.options.rating === rating) {
                    rating = void 0;
                  }
                  this.options.rating = rating;
                  this.syncRating();
                  return this.$el.trigger('starrr:change', rating);
                };

                Starrr.prototype.syncRating = function(rating) {
                  var i, _i, _j, _ref;

                  rating || (rating = this.options.rating);
                  if (rating) {
                    for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                      this.$el.find('span').eq(i).removeClass('glyphicon-star-empty').addClass('glyphicon-star');
                    }
                  }
                  if (rating && rating < 5) {
                    for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
                      this.$el.find('span').eq(i).removeClass('glyphicon-star').addClass('glyphicon-star-empty');
                    }
                  }
                  if (!rating) {
                    return this.$el.find('span').removeClass('glyphicon-star').addClass('glyphicon-star-empty');
                  }
                };

                return Starrr;

              })();
              return $.fn.extend({
                starrr: function() {
                  var args, option;

                  option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
                  return this.each(function() {
                    var data;

                    data = $(this).data('star-rating');
                    if (!data) {
                      $(this).data('star-rating', (data = new Starrr($(this), option)));
                    }
                    if (typeof option === 'string') {
                      return data[option].apply(data, args);
                    }
                  });
                }
              });
            })(window.jQuery, window);

            $(function() {
              return $(".starrr").starrr();
            });

            $( document ).ready(function() {
              $('#khong_gian').val(0);
              $('#gia_ca').val(0);
              $('#khong_gian').val(0);
              $('#phuc_vu').val(0);
              $('#chat_luong').val(0);
              $('#stars1').on('starrr:change', function(e, value){
                $('#khong_gian').val(value);
                console.log(value);
              });
              $('#stars2').on('starrr:change', function(e, value){
                $('#gia_ca').val(value);
                console.log(value);
              });
              $('#stars3').on('starrr:change', function(e, value){
                $('#phuc_vu').val(value);
                console.log(value);
              });
              $('#stars4').on('starrr:change', function(e, value){
                $('#chat_luong').val(value);
                console.log(value);
              });
            });

    </script>

  </div>

    <!-- /.container -->
    <?php
    include '../layout/footer.php'

     ?>
