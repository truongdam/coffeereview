<?php
if(!isset($_SESSION)){
    @session_start();
}
if(!isset($_SESSION['user_id'])){
  header("location:../account/login_register.php");
};
 ?>

<!DOCTYPE html>
<html lang="vi">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Review Coffee</title>
    <!-- jQuery -->
    <script src="../../public/js/jquery-3.2.0.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link href="../../public/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../public/css/style.css" rel="stylesheet">
    <link href="../../public/css/header.css" rel="stylesheet">
    <link href="../../public/css/navbar.css" rel="stylesheet">
    <link href="../../public/css/slider.css" rel="stylesheet">
    <link href="../../public/css/content.css" rel="stylesheet">
    <link href="../../public/css/footer.css" rel="stylesheet">
    <link href="../../public/css/register.css" rel="stylesheet">
    <link href="../../public/css/blog.css" rel="stylesheet">
    <link href="../../public/css/blog-content.css" rel="stylesheet">
    <link href="../../public/css/map-places.css" rel="stylesheet">
    <link href="../../public/css/place-details.css" rel ="stylesheet" >
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script language="javascript">


      $(document).ready(function(){

        $("form").submit(function (event) {

          event.preventDefault();

          $.ajax({
            url: "../../controller/create-places.php", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data){
              alert(data);
            }
          })


        })
      });
    </script>

</head>
<?php
include '../layout/header.php';
include '../layout/navbar.php'
 ?>

    <div class="container">
        <div class="row">
           <div class="row">
      <div class="col-md-12">
        <div class="well well-sm">
          <form class="form-horizontal" action="../../controller/create-places.php" method="POST" enctype="multipart/form-data">
          <fieldset>
            <legend class="text-center">Địa điểm</legend>

            <!-- Name input-->
            <div class="form-group">
              <label class="col-md-2 control-label" for="name">Tên địa điểm (*)</label>
              <div class="col-md-10">
                <input id="name" name="name" type="text" placeholder="Tên quán cafe cần review" class="form-control" required>
              </div>
            </div>

            <!-- Address input-->
            <div class="form-group">
              <label class="col-md-2 control-label" for="address">Địa chỉ (*)</label>
              <div class="col-md-10">
                <input id="address" name="address" type="text" placeholder="Địa chỉ" class="form-control" required>
              </div>
            </div>

            <!-- Description body -->
            <div class="form-group">
              <label class="col-md-2 control-label" for="description">Mô tả</label>
              <div class="col-md-10">
                <textarea class="form-control" id="description" name="description" placeholder="Vui lòng nhập mô tả về không gian, giá cả, chất lượng..." rows="5"></textarea>
              </div>
            </div>

            <!-- Description body -->
            <div class="form-group">
              <label class="col-md-2 control-label" for="description">Hình ảnh(*) </label>
              <div class="col-md-10">
                    <!-- <div class="fileinput fileinput-new" data-provides="fileinput"> -->
                        <!-- <span class="btn btn-default btn-file"> -->
                        <input id="image" name="image" type="file" / required></span>
                        <!-- <span class="fileinput-filename"></span><span class="fileinput-new">No file chosen</span> -->
                    <!-- </div> -->
              </div>
            </div>

            <!-- Form actions -->
            <div class="form-group">
              <div class="col-md-12 text-right">
                <input type="submit" class="btn btn-primary btn-md" value="Tạo địa điểm">
              </div>
            </div>
          </fieldset>
          </form>
        </div>
      </div>
    </div>
        </div>

    </div>
    <!-- /.container -->
    <?php
    include '../layout/footer.php'

     ?>
