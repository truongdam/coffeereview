<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Review Coffee</title>
    <!-- jQuery -->
    <script src="../../public/js/jquery-3.2.0.min.js"></script>
    <script src="../../public/bootstrap/js/bootstrap.min.js "></script>
    <!-- Bootstrap Core CSS -->
    <link href="../../public/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../public/css/style.css" rel="stylesheet">
    <link href="../../public/css/header.css" rel="stylesheet">
    <link href="../../public/css/navbar.css" rel="stylesheet">
    <link href="../../public/css/slider.css" rel="stylesheet">
    <link href="../../public/css/content.css" rel="stylesheet">
    <link href="../../public/css/footer.css" rel="stylesheet">
    <link href="../../public/css/register.css" rel="stylesheet">
    <link href="../../public/css/blog.css" rel="stylesheet">
    <link href="../../public/css/blog-content.css" rel="stylesheet">
    <link href="../../public/css/map-places.css" rel="stylesheet">
    <link href="../../public/css/place-details.css" rel ="stylesheet" >
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
        $(document).ready(function(){
          $("#form-search-map").submit(function (event) {

            event.preventDefault();
            var search =$("#search").val();
            console.log(search);
            $.ajax({
              url: "../../controller/map-places.php", // Url to which the request is send
              type: "POST",             // Type of request to be send, called as method
              data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
              contentType: false,       // The content type used when sending data to the server.
              cache: false,             // To unable request pages to be cached
              processData:false,
              dataType:"html",      // To send DOMDocument or non processed data file it is set to false
              success: function(data){
                //console.log(data);
                $("#content-search").html( data);
              }
            })


          })
          // -------start function-----------------------------------------//
          setTimeout(function(){
            if ($('#location-special').length){
              var location = $("#location-special").text();
                  // location = location.split(' ').join('+');
                  console.log(location);
              var url ="https://www.google.com/maps/embed/v1/place?key=AIzaSyAYg7QhrpGqjIL0DWym4uJJgXfjJnoUbHE&q="+location+",Hồ+Chí+Minh+VN";

              $("#mapGG_place").attr("src",url);
            }

          },500)
          // ------End function------------



            // -------start function-----------------------------------------//
          $("#search_place").keypress(function(e){
            if(e.which ===13){
              e.preventDefault();
              var search_place = $("#search_place").val();
                  search_place = search_place.split(' ').join('+');
              var url ="https://www.google.com/maps/embed/v1/search?key=AIzaSyAYg7QhrpGqjIL0DWym4uJJgXfjJnoUbHE&q="+search_place+",Hồ+Chí+Minh+VN";

              $("#mapGG_place").attr("src",url);

            }
          })

          // ------End function------------




          // -------start function-----------------------------------------//
          $("#store_special").click(function(e){

              e.preventDefault();
              var store_special = $("#store_special").text();
              console.log(store_special);
                  store_special = store_special.split(' ').join('+');
              var url ="https://www.google.com/maps/embed/v1/search?key=AIzaSyAYg7QhrpGqjIL0DWym4uJJgXfjJnoUbHE&q="+store_special+",Hồ+Chí+Minh+VN";

              $("#mapGG_place").attr("src",url);
          })

            // ------End function------------

          // -------start function-----------------------------------------//
          $("#location-special").click(function(e){

              e.preventDefault();
              var location = $("#location-special").text();
                  // location = location.split(' ').join('+');
                  console.log(location);
              var url ="https://www.google.com/maps/embed/v1/place?key=AIzaSyAYg7QhrpGqjIL0DWym4uJJgXfjJnoUbHE&q="+location+",Hồ+Chí+Minh+VN";

              $("#mapGG_place").attr("src",url);
          })
          // ------End function------------
        })
    </script>


</head>

<?php
include '../layout/header.php'

 ?>
 <?php
 include '../layout/navbar.php'

  ?>

    <div class="container well map-places">
            <div class="row">
                <h4 class="text-center"> Danh sách các quán cafe theo quận</h4>

            </div>
            <br>


            <div class="row search-bar">
                <div class="col-md-12">
                  <form id="form-search-map">
                    <div class="input-group">
                      <div class="input-group-btn search-panel">
                          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span id="search_concept">Lọc theo quận</span> <span class="caret"></span>
                          </button>

                          <table class="dropdown-menu table_district" >
                            <tr>
                              <td><input type="checkbox" name="address1" value="quận 1"> Quận 1 </td>
                              <td> <input type="checkbox" name="address2" value="quận 4"> Quận 4</td>
                              <td> <input type="checkbox" name="address3" value="quận 7"> Quận 7 </td>
                              <td> <input type="checkbox" name="address4" value="quận 10"> Quận 10 </td>
                              <td> <input type="checkbox" name="address5" value="quận tân bình"> Quận Tân Bình</td>

                            </tr>
                            <tr>
                              <td> <input type="checkbox" name="address6" value="quận 2">Quận 2 </td>
                              <td> <input type="checkbox" name="address7" value="quận 5"> Quận 5</td>
                              <td> <input type="checkbox" name="address8" value="quận 8"> Quận 8</td>
                              <td> <input type="checkbox" name="address9" value="quận 11"> Quận 11</td>
                              <td> <input type="checkbox" name="address10" value="quận bình thạnh"> Quận Bình Thạnh</td>
                            </tr>
                            <tr>
                              <td> <input type="checkbox" name="address11" value="quận 3"> Quận 3</td>
                              <td> <input type="checkbox" name="address12" value="quận 6"> Quận 6 </td>
                              <td> <input type="checkbox" name="address13" value="quận 9"> Quận 9 </td>
                              <td> <input type="checkbox" name="address14" value="quận 12"> Quận 12</td>
                              <td> <input type="checkbox" name="address15" value="quận thủ đức"> Quận Thủ Đức</td>

                            </tr>

                          </table>
                      </div>
                            <!-- Split button -->


                            <input type="text" class="form-control" name="search" id="search_place" placeholder="Tên quán cafe, địa chỉ ...">
                            <span class="input-group-btn">
                                <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                            </span>


                    </div>
                  </form>
                </div>
            </div>
            <div id="content-search">
            <?php
              if(isset($_GET['store_id'])){
                require '../../controller/connectDatabase.php';
                $store_id = $_GET['store_id'];
                $sql = " SELECT id, name, location,description, image FROM stores WHERE id = '" .$store_id . "'";
                $result = $conn->query($sql);
                if( $result->num_rows === 1){
                  while ( $row = $result->fetch_assoc()) {
                    $id = $row['id'];
                    $name_store = $row['name'];
                    $location = $row["location"];
                    $link_image = $row['image'];


                ?>

            <div class=" row">
                <div class="col-md-12">
                    <table class="table table-bordered">
                        <tr>
                            <th> Tên quán Coffee </th>
                            <th> Địa chỉ</th>
                        </tr>
                        <tr>
                            <td> <a id="store_special"> <?php echo $name_store ?></a> </td>
                            <td> <a id="location-special"><?php echo $location ?></a></td>

                        </tr>


                    </table>
                </div>
            </div>

            <?php
          }
          }
          }
         ?>
         </div>
        <div class="row">
            <div class="col-md-12">
                <iframe id="mapGG_place" width="100%" height="600" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAYg7QhrpGqjIL0DWym4uJJgXfjJnoUbHE&q=Đại+Học+Bách+Khoa,Hồ+Chí+Minh+VN" allowfullscreen></iframe>
            </div>
        </div>


    </div>
    <!-- /.container -->
