<!DOCTYPE html>
<html lang="vi">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php  ?></title>
    <!-- jQuery -->
    <script src="../../public/js/jquery-3.2.0.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <link href="../../public/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../../public/css/style.css" rel="stylesheet">
    <link href="../../public/css/header.css" rel="stylesheet">
    <link href="../../public/css/navbar.css" rel="stylesheet">
    <link href="../../public/css/slider.css" rel="stylesheet">
    <link href="../../public/css/content.css" rel="stylesheet">
    <link href="../../public/css/footer.css" rel="stylesheet">
    <link href="../../public/css/register.css" rel="stylesheet">
    <link href="../../public/css/blog.css" rel="stylesheet">
    <link href="../../public/css/blog-content.css" rel="stylesheet">
    <link href="../../public/css/map-places.css" rel="stylesheet">
    <link href="../../public/css/place-details.css" rel ="stylesheet" >
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="/coffeeR/public/lib/summernote-master/dist/summernote.css" rel="stylesheet">

</head>
<?php
if(!isset($_SESSION)){
    @session_start();
}
if(!isset($_SESSION['admin'])){
  header("location:../../views");
};
 ?>
<?php
include '../layout/header.php';
include '../layout/navbar.php';

 ?>
 <body>
      <div class="container">
         <div class="row">
           <div class="box">
             <form id="form" class="" action="/coffeeR/controller/create-blog.php" method="post" enctype="multipart/form-data">


               <div class="container-fluid">
                 <div class="form-group">
                     <label for="email" class="col-sm-2 control-label">
                         Tiêu đề</label>
                     <div class="col-sm-10">
                         <input type="text" class="form-control" id="tieude" name="tieude" />
                     </div>
                 </div>
                 <br>
                 <div class="form-group">
                     <label for="mobile" class="col-sm-2 control-label">
                         Tóm tắt</label>
                     <div class="col-sm-10">
                         <input type="text" class="form-control" id="tomtat" name="tomtat" />
                     </div>
                 </div>
                 <br>
                 <div class="form-group">
                     <label for="mobile" class="col-sm-2 control-label">
                         Ảnh đại diện</label>
                     <div class="col-sm-10">
                         <input type="file" class="form-control" id="hinhanh" name="hinhanh" />
                     </div>
                 </div>
               </div>
               <br>
               <br>
               <input type="hidden" name="noidung" id="noidung" value="">
               <div class="container-fluid">

                 <div class="form-group">
                   <div class="col-sm-10" id="summernote"></div>
                 </div>
                 <div class="col-md-1 col-md-offset-5" >
                   <button class="btn btn-success"type="submit" id="submit" name="submit">Lưu bài viết</button>

                 </div>


               </div>
              </form>
           </div>
         </div>
      </div>

<?php
      include('../layout/register.php');
      include('../layout/footer.php')
 ?>
 <a href="javascript:void(0)" onclick="jQuery('html,body').animate({scrollTop: 0},1000);" class="go_top" style=""></a>
 <!-- Bootstrap Core JavaScript -->
  <script src="/coffeeR/public/js/jquery-2.1.4.js"></script>
 <script src="../../public/bootstrap/js/bootstrap.min.js"></script>
 <script src="../../public/js/banner/jssor.slider-22.2.16.mini.js" type="text/javascript"></script>
 <!-- Insert to your webpage before the </head> -->

    <script src="http://uguru-realestate-us-jun202013.businesscatalyst.com/3d-slider/sliderengine/amazingslider.js"></script>
    <script src="http://uguru-realestate-us-jun202013.businesscatalyst.com/3d-slider/sliderengine/initslider-1.js"></script>
    <!-- End of head section HTML codes -->
    <script src="/coffeeR/public/lib/summernote-master/dist/summernote.js"></script>
    <script>
      $(document).ready(function() {

          $('#summernote').summernote({
            height: 300,                 // set editor height
            minHeight: null,             // set minimum height of editor
            maxHeight: null,             // set maximum height of editor
            focus: true                  // set focus to editable area after initializing summernote
          });
          // $('#summernote').change(function(){
          //   $('#noidung').val($('#summernote').summernote('code'));
          // });
          $('#submit').click(function(){

            $('#noidung').val($('#summernote').summernote('code'));

            //  $('form').submit();


            // alert($('#noidung').val());


            // $.ajax({
            //   type: "POST",
            //   url: $(this).attr('action'),
            //   data: new FormData(this)
            //     // tieude : $('#tieude').val(),
            //     // tomtat : $('#tomtat').val(),
            //     // hinhanh: $('#hinhanh').val(),
            //     // noidung: $('#summernote').summernote('code')
            //   ,
            //   success: function(d){alert(d);
            //
            //   },
            //   error: function(d){alert(d);
            //
            //   }
            // });
          });

      });
    </script>
</body>
</html>
