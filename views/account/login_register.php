<!-- Large modal -->


<!DOCTYPE html>
<html lang="vi">
<head>


  <link href="../../public/css/style.css" rel="stylesheet">
  <link href="../../public/css/header.css" rel="stylesheet">
  <link href="../../public/css/navbar.css" rel="stylesheet">
  <link href="../../public/css/slider.css" rel="stylesheet">
  <link href="../../public/css/content.css" rel="stylesheet">
  <link href="../../public/css/footer.css" rel="stylesheet">
  <link href="../../public/css/register.css" rel="stylesheet">
  <link href="../../public/css/blog.css" rel="stylesheet">
  <link href="../../public/css/blog-content.css" rel="stylesheet">
  <link href="../../public/css/map-places.css" rel="stylesheet">
  <link href="../../public/css/place-details.css" rel ="stylesheet" >

  <link href="../../public/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <script src="../../public/js/jquery-3.2.0.min.js"></script>
  <script src="../../public/bootstrap/js/bootstrap.min.js"></script>
  <script language="javascript">


    $(document).ready(function(){

      $("#form-register").submit(function (event) {

        event.preventDefault();

        $.ajax({
          url: "../../controller/register.php", // Url to which the request is send
          type: "POST",             // Type of request to be send, called as method
          data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
          contentType: false,       // The content type used when sending data to the server.
          cache: false,             // To unable request pages to be cached
          processData:false,        // To send DOMDocument or non processed data file it is set to false
          success: function(data){
            alert(data);
          }
        })


      })
    });
  </script>
</head>

  <?php include '../layout/header.php'; ?>
  <?php include '../layout/navbar.php'; ?>

    <div class="" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        ×</button>
                    <h4 class="modal-title" id="myModalLabel">
                        Login/Registration</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-8" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#Login" data-toggle="tab">Login</a></li>
                                <li><a href="#Registration" data-toggle="tab">Registration</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="Login">
                                    <form role="form" action="../../controller/login.php" method="POST" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="username" class="col-sm-2 control-label">
                                            User</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name ="username" id="username" placeholder="User name" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1" class="col-sm-2 control-label">
                                            Password</label>
                                        <div class="col-sm-10">
                                            <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" />
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary btn-sm">
                                                Submit</button>
                                            <a href="javascript:;">Forgot your password?</a>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                                <div class="tab-pane" id="Registration">
                                    <form  role="form" class="form-horizontal" id="form-register" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="username2" class="col-sm-2 control-label">
                                            User</label>
                                            <div class="col-sm-10">
                                                <input type="text" name="username" class="form-control" id="username2" placeholder="User name" required />
                                            </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password" class="col-sm-2 control-label">
                                            Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" name="password" pattern="[A-Za-z0-9]{4,.}" class="form-control" id="password" placeholder="Password" required />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-sm-2 control-label">
                                            Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" name ="email" class="form-control" id="email" placeholder="Email" required />
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="image" class="col-sm-2 control-label">
                                            Image</label>
                                        <div class="col-sm-10">
                                            <input type="file" name ="image" class="" id="image" placeholder="Please choose your avatar" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-10">
                                          Password at least 4 characters.
                                        </div>
                                    </div>


                                    <div class="row">
                                        <div class="col-sm-2">
                                        </div>
                                        <div class="col-sm-10">
                                            <button type="submit" class="btn btn-primary btn-sm">
                                                Save & Continue</button>
                                            <button type="reset" class="btn btn-default btn-sm">
                                                Reset</button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                            <div id="OR" class="hidden-xs">
                                OR</div>
                        </div>
                        <div class="col-md-4">
                            <div class="row text-center sign-with">
                                <div class="col-md-12">
                                    <h3>
                                        Sign in with</h3>
                                </div>
                                <div class="col-md-12">
                                    <div class="btn-group btn-group-justified">
                                        <a href="#" class="btn btn-primary">Facebook</a> <a href="#" class="btn btn-danger">
                                            Google</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  <?php include '../layout/footer.php'; ?>



</html>
