<!DOCTYPE html>
<html lang="vi">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Review Coffee</title>
    <!-- jQuery -->
    <link href="../../public/css/style.css" rel="stylesheet">
    <link href="../../public/css/header.css" rel="stylesheet">
    <link href="../../public/css/navbar.css" rel="stylesheet">
    <link href="../../public/css/slider.css" rel="stylesheet">
    <link href="../../public/css/footer.css" rel="stylesheet">


    <link href="../../public/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <script src="../../public/js/jquery-3.2.0.min.js"></script>
    <script src="../../public/bootstrap/js/bootstrap.min.js"></script>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script language="javascript">


      $(document).ready(function(){
        $("#notice").hide();

        $("form").submit(function (event) {

          event.preventDefault();

          $.ajax({
            url: "../../controller/account.php", // Url to which the request is send
            type: "POST",             // Type of request to be send, called as method
            data: new FormData(this), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
            contentType: false,       // The content type used when sending data to the server.
            cache: false,             // To unable request pages to be cached
            processData:false,        // To send DOMDocument or non processed data file it is set to false
            success: function(data){
              $("#notice strong").html(data);
              $("#notice").show();

            }
          })


        })
      });
    </script>

</head>
<body>
  <?php include "../layout/header.php" ?>
  <?php include "../layout/navbar.php" ?>
<div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 toppad" >


          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">Profile Information</h3>
            </div>
            <form  enctype="multipart/form-data">
            <div class="panel-body">
              <div class="row">
                <div id="notice" class="col-lg-12 col-md-12 alert alert-success" role="alert">
                    <strong >Well done!</strong>
                </div>
              </div>

              <?php
              require '../../controller/connectDatabase.php';
              if(!isset($_SESSION)){
                  session_start();
              }
              if(isset($_SESSION['user_id'])){
                $user_id =$_SESSION['user_id'];
                  $sql = " SELECT id, name, image, email FROM users WHERE id = '" .$user_id . "'";
                  $result = $conn->query($sql);
                  if( $result->num_rows === 1){
                    while ( $row = $result->fetch_assoc()) {
                      $user_name = $row['name'];
                      $email = $row["email"];
                      $link_image = $row['image'];
                    }
                    ?>

              <div class="row">
                <div class="col-md-2 col-lg-2 1" align="center">
                  <img alt="User Pic" src="<?php echo $link_image ?>" class="img-circle img-responsive">
                  <br>
                  <input type="file" name="image">
                </div>

                <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                  <dl>
                    <dt>DEPARTMENT:</dt>
                    <dd>Administrator</dd>
                    <dt>HIRE DATE</dt>
                    <dd>11/12/2013</dd>
                    <dt>DATE OF BIRTH</dt>
                       <dd>11/12/2013</dd>
                    <dt>GENDER</dt>
                    <dd>Male</dd>
                  </dl>
                </div>-->
                <div class="col-lg-1"></div>
                <div class=" col-md-9 col-lg-9 ">
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Username:</td>
                        <td><?php echo $user_name  ?></td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="mailto:info@support.com"><?php echo $email  ?></a></td>
                      </tr>
                      <tr>
                        <td>New Password:</td>
                        <td><input type="password" name="password"></td>
                      </tr>
                      <tr>
                        <td>Phone Number</td>
                        <td>            </td>

                      </tr>

                    </tbody>
                  </table>

                  <button type="submit" class="btn btn-danger">Save</button>
                  <button type="reset" class="btn btn-warning">Reset</button>
                </div>
              </div>

              <?php
            }
        }

         ?>
            </div>
          </form>
                 <div class="panel-footer">

                        <span class="pull-right">

                        </span>
                    </div>

          </div>
        </div>
      </div>
    </div>

    <?php include "../layout/footer.php" ?>
    <body>
