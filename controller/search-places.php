<?php
  require 'connectDatabase.php';
  if ($_SERVER['REQUEST_METHOD'] === "POST") {
      $diachi =array();
      $in = -1;
      for ($i=1;$i<=15;$i++){
        if(isset($_POST["address".$i])){
          $in=$in+1;
          $diachi[$in]=$_POST["address".$i];
        };
      }

      if(isset($_POST["search"])){
        $search = $_POST["search"];
      }
      else {
        # code...
        $search="";
      }


            $query1=NULL;
            if($search ===""){
              $query1=NULL;
            }
            else {
              $query1 = "(name LIKE '%".$search. "%' OR location LIKE '%".$search. "%')";
            }

            $query2=NULL;
            if($in>=1){
              $query2 = "location LIKE '%".$diachi[0]. "%'";
              for ($k=1;$k<= $in;$k++){
                $query2 = $query2 ." OR location LIKE '%".$diachi[$k]. "%'";
              }
            }
            elseif($in===0){
              $query2 = "location LIKE '%".$diachi[0]. "%'";
            }
            else{
              $query2 =NULL;
            }
      if($query1!==NULL && $query2!==NULL){
          $sql = "SELECT * FROM Stores WHERE ".$query1. " AND (" .$query2.")";
      }
      elseif($query1!==NULL) {
          $sql = "SELECT * FROM Stores WHERE ".$query1;
      }
      elseif($query2!==NULL) {
          $sql = "SELECT * FROM Stores WHERE ".$query2;
      }
      else {
        $sql = "SELECT * FROM Stores";
      }

      $result = $conn->query($sql);
      if( $result->num_rows){
        while ( $row = $result->fetch_assoc()) {
          $id = $row['id'];
          $name_store = $row['name'];
          $location = $row["location"];
          $link_image = $row['image'];
          $score_rank = $row['score_rank'];
          $num_ranker = $row['num_ranker'];
          $brief_location ="";
          if(strlen($location)>65){
            $brief_location = substr($location,0,strpos($location,'TP')) . "..." ;
          }
          else {
            $brief_location =$location;
          }
?>
<div class="col-lg-4" >
  <div class="thumbnail" id="list-store">
      <img src=" <?php echo $link_image ; ?> " alt="<?php echo $name_store ?>">
      <div class="caption">
        <div class="name-place">
          <a href="./place/map-places.php?store_id=<?php echo $id ?>"> <?php echo $name_store; ?> </a>
        </div>
        <div class="address-place"> <?php echo $brief_location; ?></div>
        <div class="info-place">
          <div class="three-col">
            <span class="glyphicon glyphicon-star" aria-hidden="true"></span>: <?php echo $score_rank; ?>
          </div>
          <div class="three-col">
            <span class="glyphicon glyphicon-user " aria-hidden="true"></span>: <?php echo $num_ranker; ?>
          </div>
          <div class="three-col align-right">
            <a href="./place/place-details.php?id= <?php echo $id ?>"  class="btn btn-default btn-sm" role="button">Chi tiết</a>
          </div>
        </div>
      </div>
    </div>
</div>
<?php
        }

      }

  }
?>
