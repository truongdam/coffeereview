<?php
  require 'connectDatabase.php';

  if ($_SERVER['REQUEST_METHOD'] == "POST") {
    $bool_login = false;
    $username = $_POST['username'];
    $password = $_POST['password'];

    $sql = "SELECT * FROM users WHERE name='" . $username . "'";
    $result = $conn->query($sql);
    if (($result->num_rows)===1) {
      $row = $result->fetch_assoc();
      $hashpass = $row['pass'];
      $user_id = $row['id'];
      $user_name =$row['name'];
      $role = $row['role'];
      if (password_verify($password,$hashpass)){
          $bool_login = TRUE;
      }
      else {
          $bool_login = FALSE;
      }

    }
    else {
      $bool_login = FALSE;
    }

    if($bool_login){
      session_start();
      $_SESSION['user_id']=$user_id;
      $_SESSION['username']=$user_name;
      if($role==="ADMIN") $_SESSION['admin'] =$user_id;
      header("Location:../views");
    }
    else {
      header("Location:../views/account/login_register.php");
    };
  }
 ?>
