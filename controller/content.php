<?php
  require 'connectDatabase.php';
  $sql = " SELECT id, name, location, image, num_viewer, num_ranker, score_rank FROM stores ";
  $result = $conn->query($sql);
  if( $result->num_rows){
    while ( $row = $result->fetch_assoc()) {
      $id = $row['id'];
      $name_store = $row['name'];
      $location = $row["location"];
      $link_image = $row['image'];
      $score_rank = $row['score_rank'];
      $num_viewer = $row['num_viewer']
      # code...
  ?>
      <div class="col-lg-4" >
        <div class="thumbnail" id="list-store">
            <img src=" <?php echo $link_image ; ?> " alt="">
            <div class="caption">
              <div class="name-place">
                <a href=""> <?php echo $name_store; ?> </a>
              </div>
              <div class="address-place"> <?php echo $location; ?></div>
              <div class="info-place">
                <div class="three-col">
                  <span class="glyphicon glyphicon-stats" aria-hidden="true"></span>: <?php echo $score_rank; ?>
                </div>
                <div class="three-col">
                  <span class="glyphicon glyphicon-user " aria-hidden="true"></span>: <?php echo $num_viewer; ?>
                </div>
                <div class="three-col align-right">
                  <a href="/place-details" class="btn btn-default btn-sm" role="button">Chi tiết</a>
                </div>
              </div>
            </div>
          </div>
      </div>

  <?php
    }
  }

 ?>
